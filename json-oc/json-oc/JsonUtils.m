//
//  JsonUtils.m
//  json-oc
//
//  Created by shk on 15/1/7.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import "JsonUtils.h"

@implementation JsonUtils

+ (NSString *) objToJson:(id) obj{
    NSDictionary* objDict = [self objectToDictionary:obj];
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:objDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *json = [[NSString alloc] initWithData:data  encoding:NSUTF8StringEncoding];
    return json;
}

+ (id) jsonToObj:(NSString *) json cls:(Class)cls{
    return [self jsonToObj:json cls:cls subclass:nil];
}

+ (id) jsonToObj:(NSString *) json cls:(Class)cls subclass:(NSDictionary *) subclass{
    NSData* jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    return [self dictionaryToObject:jsonDict cls:cls subclass:subclass];
}







+ (id) dictionaryToObject:(NSDictionary *) dict cls:(Class)cls subclass:(NSDictionary *) subclass{
    id obj = [[cls alloc] init];
    NSDictionary *properties = [ReflectUtils getPropertiesForClassWithSupperClass:[obj class]];
    for (NSString *propertyName in properties) {
        id propertyValue = dict[propertyName];
        if(propertyValue != nil){
            NSString *propertyClassName = [properties objectForKey:propertyName];
            if([propertyValue isKindOfClass:[NSDictionary class]]){//如果属性是自定义对象
                Class propertyObjClass = [ReflectUtils getEntityByClassName:propertyClassName];//自定义对象属性的类实体
                id propertyObj = [self dictionaryToObject:propertyValue cls:propertyObjClass subclass:subclass];
                [obj setValue: propertyObj forKey:propertyName];
            }if([propertyValue isKindOfClass:[NSMutableArray class]]){//如果属性是集合类型
                NSString *subclassName = subclass[propertyName];
                
                NSMutableArray *propertyValues = [[NSMutableArray alloc] init];
                NSMutableArray *propertyValueArray = propertyValue;
                
                Class propertyObjClass = [ReflectUtils getEntityByClassName:subclassName];//集合内的对象的类实体
                for (NSDictionary *dictObjInArray in propertyValueArray) {
                    id propertyObj = [self dictionaryToObject:dictObjInArray cls:propertyObjClass subclass:subclass];
                    [propertyValues addObject:propertyObj];
                }
                [obj setValue: propertyValues forKey:propertyName];
            }else if([self isSupportClass:propertyClassName]){
                [obj setValue:propertyValue forKey:propertyName];
            }
        }
    }
    return obj;
}

+ (NSDictionary *) objectToDictionary:(id) obj{
    NSDictionary *properties = [ReflectUtils getPropertiesForClassWithSupperClass:[obj class]];
    NSMutableDictionary* objDict = [NSMutableDictionary dictionary];
    for (NSString* propertyName in properties) {
        NSString *propertyClassName = [properties objectForKey:propertyName];
        id val = [obj valueForKey:propertyName];
        if([self isSupportClass:propertyClassName]){//如果是基本数据类型
            [objDict setValue:val forKey:propertyName];
        } else if([propertyClassName isEqualToString:@"NSMutableArray"]){//如果是数组
            NSMutableArray *objValueDictArray = [[NSMutableArray alloc] init];
            NSMutableArray *objValueArray = val;
            for (NSObject *object in objValueArray) {
                [objValueDictArray addObject:[self objectToDictionary:object]];
            }
            [objDict setValue:objValueDictArray forKey:propertyName];
        } else {//如果是自定义对象
            [objDict setValue:[self objectToDictionary:val] forKey:propertyName];
        }
    }
    return [NSDictionary dictionaryWithDictionary:objDict];;
}

+ (BOOL) isSupportClass:(NSString *) className{
    NSArray *systemClass = @[@"NSString", @"NSNumber", @"NSInteger", @"NSDate", @"BOOL"];
    return [systemClass containsObject:className];
}

@end
