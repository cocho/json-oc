//
//  JsonUtils.h
//  json-oc
//
//  Created by shk on 15/1/7.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReflectUtils.h"

@interface JsonUtils : NSObject


/**
 *  将对象转换为Json字符串
 *
 *  @param obj 需要被序列化的对象（可以包含基本数据类型的属性、自定义类型的属性和数组类型的属性）
 *  @param
 *  @return json序列化成功后的字符串
 */
+ (NSString *) objToJson:(id) obj;

/**
 *  将Json字符串转换为简单对象（不包含属性类型为集合的属性）
 *
 *  @param  json    json字符串（可以包含基本数据类型的属性、自定义类型的属性和数组类型的属性）
 *  @param  cls     反序列化的目标类对象
 *  @return 通过字符串反序列化出的对象
 */
+ (id) jsonToObj:(NSString *) json cls:(Class)cls;

/**
 *  将Json字符串转换为复杂对象（包含属性类型为集合类型的属性）
 *
 *  @param  json        json字符串（可以包含基本数据类型的属性、自定义类型的属性和数组类型的属性）
 *  @param  cls         反序列化的目标类对象
 *  @param  subclass    反序列化的目标类对象中自定义属性类型与名称的对应关系
 *  @return 通过字符串反序列化出的对象
 */
+ (id) jsonToObj:(NSString *) json cls:(Class)cls subclass:(NSDictionary *) subclass;




@end
