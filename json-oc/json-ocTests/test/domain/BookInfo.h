//
//  BookInfo.h
//  json-oc
//
//  Created by shk on 15/1/8.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookInfo : NSObject

@property (nonatomic, retain) NSNumber* bookId;

@property (nonatomic, retain) NSString* bookName;


@end
