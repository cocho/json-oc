//
//  UserDetailInfo.h
//  json-oc
//
//  Created by shk on 15/1/8.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetailInfo : NSObject

@property (nonatomic, retain) NSString* address;

@property (nonatomic, retain) NSNumber* phone;


@end
