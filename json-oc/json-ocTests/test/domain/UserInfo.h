//
//  UserInfo.h
//  json-oc
//
//  Created by shk on 14/12/31.
//  Copyright (c) 2014年 com.shk.study. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDetailInfo.h"
#import "BookInfo.h"

@interface UserInfo : NSObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSNumber* age;

@property (nonatomic, retain) UserDetailInfo* detail;

@property (nonatomic, retain) NSMutableArray* books;


@end
