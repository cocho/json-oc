//
//  JsonFromObjTest.m
//  json-oc
//
//  Created by shk on 15/1/8.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "UserInfo.h"
#import "JsonUtils.h"
#import "UserDetailInfo.h"
#import "BookInfo.h"

@interface JsonFromObjTest : XCTestCase

@end

@implementation JsonFromObjTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testObjSmpleToJson{
    UserInfo *u1 = [[UserInfo alloc] init];
    u1.email = @"songhaikang@163.com";
    u1.age = [NSNumber numberWithInt: 11];
   
    NSString *json = [JsonUtils objToJson:u1];
    NSLog(@"json转换结果\n%@", json);
}


- (void) testObjWithObjPropertyToJson{
    UserInfo *u1 = [[UserInfo alloc] init];
    u1.email = @"songhaikang@163.com";
    u1.age = [NSNumber numberWithInt: 11];
    
    UserDetailInfo *detail = [[UserDetailInfo alloc] init];
    detail.address = @"南阳市";
    detail.phone = [NSNumber numberWithLong:15000814265];
    
    u1.detail = detail;
    
    NSString *json = [JsonUtils objToJson:u1];
    NSLog(@"json转换结果\n%@", json);
}

- (void) testObjWithArrayPropertyToJson{
    UserInfo *u1 = [[UserInfo alloc] init];
    u1.email = @"songhaikang@163.com";
    u1.age = [NSNumber numberWithInt: 11];
    
    UserDetailInfo *detail = [[UserDetailInfo alloc] init];
    detail.address = @"南阳市";
    detail.phone = [NSNumber numberWithLong:15000814265];
    u1.detail = detail;
    
    NSMutableArray *books = [[NSMutableArray alloc] init];
    BookInfo *book1 = [[BookInfo alloc] init];
    book1.bookId = [NSNumber numberWithInt:1];
    book1.bookName = @"人性的弱点";
    [books addObject:book1];
    
    BookInfo *book2 = [[BookInfo alloc] init];
    book2.bookId = [NSNumber numberWithInt:2];
    book2.bookName = @"狼性管理团队";
    [books addObject:book2];
    u1.books = books;
    
    
    NSString *json = [JsonUtils objToJson:u1];
    NSLog(@"json转换结果\n%@", json);
}








- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
