//
//  JsonToObjTest.m
//  json-oc
//
//  Created by shk on 15/1/8.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "UserInfo.h"
#import "JsonUtils.h"
#import "UserDetailInfo.h"
#import "BookInfo.h"

@interface JsonToObjTest : XCTestCase

@end

@implementation JsonToObjTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testJsonToSimpleObj{
    NSString *json = @"{\"name\":\"shk\", \"age\":22}";
    UserInfo *user = [JsonUtils jsonToObj:json cls:[UserInfo class]];
    NSLog(@"对象中取出的值：name:%@，age:%@", user.name, user.age);
}

- (void) testJsonToObjWithObjProperty{
    NSString *json = @"{\"email\" : \"songhaikang@163.com\",\"age\" : 11,\"detail\" : {\"address\" : \"南阳市\",\"phone\" : 15000814265}}";
    UserInfo *user = [JsonUtils jsonToObj:json cls:[UserInfo class]];
    UserDetailInfo *detail = user.detail;
    NSLog(@"对象中取出的值：email:%@，age:%@，name:%@    address:%@ ", user.email, user.age, user.name, detail.address);
}

- (void) testJsonToObjWithArrayProperty{
    NSString *json = @"{\"email\" : \"songhaikang@163.com\",\"age\" : 11,\"detail\" : {\"address\" : \"南阳市\",\"phone\" : 15000814265},\"books\" : [{\"bookId\" : 1,\"bookName\" : \"人性的弱点\"},{\"bookId\" : 2,\"bookName\" : \"狼性管理团队\"}]}";
    NSDictionary *subclass = [NSDictionary dictionaryWithObjectsAndKeys:@"BookInfo", @"books",nil];
    UserInfo *user = [JsonUtils jsonToObj:json cls:[UserInfo class] subclass:subclass];
    UserDetailInfo *detail = user.detail;
    NSLog(@"对象中取出的值：email:%@，age:%@，name:%@    address:%@ ", user.email, user.age, user.name, detail.address);
    NSMutableArray *books = user.books;
    for (BookInfo *book in books) {
        NSLog(@"课程名称bookName：%@  bookId：%@", book.bookName, book.bookId);
    }
}


- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
