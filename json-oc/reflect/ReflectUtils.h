//
//  ReflectUtils.h
//  json-oc
//
//  Created by shk on 15/1/7.
//  Copyright (c) 2015年 com.shk.study. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ReflectUtils : NSObject


/**
 *  根据类名获得实体对象
 *
 *  @param className 类名
 *  @return 通过反射获得的实体对象
 */
+ (Class) getEntityByClassName:(NSString *)className;

/**
 *  根据实体对象获得类名
 *
 *  @param classEntity 实体对象
 *  @return 类名
 */
+ (NSString *) getClassNameByClass:(Class)classEntity;


/**
 *  根据实体类获得实体类的属性集合
 *
 *  @param cls 实体类
 *  @return 属性集合（泛型为objc_property_t类型的对象）
 */
+ (NSArray*)getPropertysByClass:(Class) cls;

/**
 *  根据实体类获得实体属性的名称集合
 *
 *  @param cls 实体类
 *  @return 属性名称集合（泛型为字符串类型）
 */
+ (NSArray*)getPropertyNamesByClass:(Class) cls;

/**
 *  根据实体类获得实体属性的字典集合
 *
 *  @param cls 实体类
 *  @return 属性的字典集合（键：属性名称，值：属性类型字符串）
 */
+ (NSDictionary *)getPropertiesForClass:(Class)cls;

/**
 *  根据实体类获得实体（包含父类）属性的字典集合
 *
 *  @param cls 实体类
 *  @return 属性的字典集合（键：属性名称，值：属性类型字符串）
 */
+ (NSDictionary *)getPropertiesForClassWithSupperClass:(Class)cls;


/**
 *  根据根据属性对象获得属性类型（该函数为网上找的）
 *
 */
+ (const char *) getPropertyType :(objc_property_t) property;

@end
